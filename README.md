### py-sketchy-hachures

Python adaptation of Andy's Woodruff javascript code to generate hachures-like maps.

Original project link: [sketchy-hachures](https://github.com/awoodruff/sketchy-hachures)

For further explanations: [hachures-and-sketchy-relief-maps](http://andywoodruff.com/blog/hachures-and-sketchy-relief-maps/)

![bigisland_sketchy](bigisland_sketchy_art.jpg)