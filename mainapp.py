'''
MIT License

Copyright (c) 2019 Ivan D'Ortenzio

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Based on original work of Andy Woodruff (@awoodruff)

https://github.com/awoodruff/sketchy-hachures

Created on 21 feb 2019

@author: Ivan D'Ortenzio
'''
import os
import cv2
import math
import random
import numpy as np
import cairo

def range1(start, end,step=1):
    return range(start, end + 1,step)

def np_arange1(start, end,step=1):
    return np.arange(start, end + 1,step)

def offset(n):
    r = random.random()
    if (r > 0.66):
        return n + 1;
    if (r < 0.33):
        return n - 1;
    return n;

def drawRelief(in_img):
    ''' This function returns a list of pixels with slope and aspect associated  
    [[x0,y0,slope0,aspect0],
     [x1,y1,slope1,aspect1],
     [...]]
    '''
    
    data = []
    for xx in range(strokeLength, (width - strokeLength), cellSize):
        for yy in range(strokeLength, (height - strokeLength), cellSize):
            
            # random offset of x/y
            x = offset(xx);
            y = offset(yy);
            
            # read the pixel value at xy
            pxval = in_img[y, x]
            
            # assume value 0 (black) is water       
            if pxval < 1:
                data.append([x, y, -9999, -9999])  # we assign -9999 to slope and aspect
                continue
            
            '''
            now get average values for pixels to the top left, top, top right, etc. of center pixel
            ... except instead of single pixels, work with average value of 3x3 cells
            
            AAA  BBB  CCC
            AAA  BBB  CCC
            AAA  BBB  CCC
            DDD  EEE  FFF
            DDD  E-E  FFF
            DDD  EEE  FFF
            GGG  HHH  III
            GGG  HHH  III
            GGG  HHH  III
            
            where '-' represents our current pixel. for top left value we'll use average of the 'A' cells, and so on.
            '''
           
            gridSize = 3;
            squaredCell = float(gridSize * gridSize);
            halfCell = math.floor(cellSize / 2);
            
            cells = {0:{0:0, 1:0, 2:0},
                     1:{0:0, 1:0, 2:0},
                     2:{0:0, 1:0, 2:0}}
            
            for row in range1(-1, 1):
                for col in range1(-1, 1):
                    if (row == 0 and col == 0):
                        cells[row + 1][col + 1] = None
                        continue
                    cx = x + col * gridSize
                    cy = y + row * gridSize;
                    avg = 0;           
                    for cellX in np_arange1(cx - halfCell, cx + halfCell + 1):
                        for cellY in np_arange1(cy - halfCell, cy + halfCell + 1):
                            
                            # read the pixel value at xy                          
                            pxval = in_img[cellY, cellX]
                            if pxval:
                                avg += pxval;
                    avg /= squaredCell;
                    cells[row + 1][col + 1] = avg;
                    
            '''
            http://desktop.arcgis.com/en/arcmap/10.3/tools/spatial-analyst-toolbox/how-aspect-works.htm
            
            A  B  C
            D  E  F
            G  H  I
            
            [dz/dx] = ((c + 2f + i) - (a + 2d + g)) / 8
            [dz/dy] = ((g + 2h + i) - (a + 2b + c)) / 8
            
            aspect = 57.29578 * atan2 ([dz/dy], -[dz/dx])
            if aspect < 0
                cell = 90.0 - aspect  else if aspect > 90.0
                cell = 360.0 - aspect + 90.0
            else
                cell = 90.0 - aspect
            '''
                    
            a = cells[0][0]
            b = cells[0][1]
            c = cells[0][2]
            d = cells[1][0]
            f = cells[1][2]
            g = cells[2][0]
            h = cells[2][1]
            i = cells[2][2]
            
            dx = ((c + 2 * f + i) - (a + 2 * d + g)) / 8
            dy = ((g + 2 * h + i) - (a + 2 * b + c)) / 8
            aspect = 57.29578 * math.atan2(dy, -dx)
            if (aspect < 0):
                aspect = 90.0 - aspect 
            elif (aspect > 90.0):
                aspect = 360.0 - aspect + 90.0
            else:
                aspect = 90.0 - aspect;
            aspect /= 57.29578;

            slx = (f - d) / 9;
            sly = (h - b) / 9;
            sl0 = math.sqrt(slx * slx + sly * sly);
            if (np.isnan(sl0)):
                continue;
            
            # save slope and azimuth for this pixel
            data.append([x, y, sl0, aspect]);

    return data

            
def draw(in_data):
    ''' This function draw an hachure-like map to a png file using pycairo engine.
    Precompiled package of pycairo can be downloaded from here:
    https://www.lfd.uci.edu/~gohlke/pythonlibs/#pycairo
    '''
    with cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height) as surface:
        
        # To draw with cairo we create a context and set the target surface
        context = cairo.Context(surface)       
        # ...and we fill the background with white color
        context.set_source_rgba(255, 255, 255, 1.0)        
        # Draw a rectangle of the input image dimensions
        context.rectangle(0, 0, width, height)      
        # Fill the rectangle with white color
        context.fill()
        
        # draws image several more times on top of itself with varying sun azimuths
        for sunAzimuth in azimuths:
            for pt in in_data:
                
                # This means water. Remember when we assigned -9999 to slope and aspect...
                if (pt[2] == -9999):
                    
                    # stroke goes sw-ne, with a little random variation
                    dt = [pt[0], pt[1], math.pi * .65 + (random.random() * (0.2 * math.pi))]
                    lineWidth = .08
                    strokeColor = waterColor                    
                else:
                    dt = [pt[0], pt[1], pt[3] + math.pi / 2]
                    
                    # luminance       
                    L = math.cos(pt[3] - sunAzimuth) * math.cos(math.pi * .5 - math.atan(pt[2])) * math.cos(sunElev) + math.sin(math.pi * .5 - math.atan(pt[2])) * math.sin(sunElev);
                    if (L < 0):
                        L = 0;
                    
                    # based on slope            
                    widthClass = min(int(math.atan(pt[2]) / (1.0 / numberOfWidths)), numberOfWidths - 1);
                    
                    # based on luminance         
                    colorClass = int(min(math.floor(L * len(colors)), len(colors) - 1))
                    lineWidth = max(.08, .08 * widthClass)
                    strokeColor = colors[colorClass]
                     
                x = dt[0]
                y = dt[1]
                d = (strokeLength) / 2;
                az = dt[2]
                
                # The start points
                x0 = -d * math.cos(az) + x;
                y0 = -d * math.sin(az) + y;
                
                # The controll points
                x1 = x + (2 - random.random() * 2)
                y1 = y + (2 - random.random() * 2)
                
                # The endpoints 
                x2 = d * math.cos(az) + x;
                y2 = d * math.sin(az) + y;
                               
                context.set_line_width(lineWidth)
                context.set_source_rgba(strokeColor[0], strokeColor[1], strokeColor[2], strokeColor[3])
                
                # context.line_to(x2,y2)
                
                ''' 
                I don't know if this is the best way to draw a quadratic bezier curve...
                https://cairo.cairographics.narkive.com/cg4pzFhO/a-quadratic-bezier-curve
                Use the line_to command above instead if you don't want this...
                '''
                context.move_to(x0, y0)
                context.curve_to(2.0 / 3.0 * x1 + 1.0 / 3.0 * x0,
                  2.0 / 3.0 * y1 + 1.0 / 3.0 * y0,
                  2.0 / 3.0 * x1 + 1.0 / 3.0 * x2,
                  2.0 / 3.0 * y1 + 1.0 / 3.0 * y2, x2, y2)
                # draw the shape    
                context.stroke()
        # write the surface to png file
        surface.write_to_png(out_png)                    

        
# The input elevation data
src_img = 'bigisland.png'
# src_img = 'sf.png';
# src_img = 'oahu.png';
# src_img = 'molokai.png';
# src_img = 'craterlake_black.png';
# src_img = 'mtwashington.png';
# src_img = 'yosemite.png';

# The output image result
out_png = os.path.splitext(src_img)[0] + "_sketchy.png"

strokeLength = 10;
cellSize = 2;
numberOfWidths = 10;
sunElev = math.pi * 0.25;
azimuths = [1.5 * math.pi, 2 * math.pi, 1.75 * math.pi]

# colors correspond to shadowing, in order from dark to light. (these are some greens from ColorBrewer)
colors = [[0.00000, 0.27059, 0.16078, 0.35], [0.00000, 0.40784, 0.21569, 0.35], [0.13725, 0.51765, 0.26275, 0.35], [0.25490, 0.67059, 0.36471, 0.35], [0.47059, 0.77647, 0.47451, 0.35]];
waterColor = [0.12941, 0.44314, 0.70980, 0.75];

# load elevation image data using cv2 library
img = cv2.imread(src_img, 0)   
height, width = img.shape

data = drawRelief(img)
draw(data)  # draw map!
